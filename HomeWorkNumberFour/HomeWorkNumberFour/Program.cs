﻿using System;
using static System.Console;

namespace HomeWorkNumberFour
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Введите количество строк для двух матриц.");
            int lines = int.Parse(ReadLine());
            WriteLine("Введите количество столбцов для двух матриц.");
            int columns = int.Parse(ReadLine());
            WriteLine($"Ввидите с какого числа начинается подбор случайных чисел.");
            int rundMin = int.Parse(ReadLine());
            WriteLine("Ввидите на каком числе заканчивается подбор случайных чисел.");
            int rundMax = int.Parse(ReadLine());
            rundMax += 1;

            int[,] arrayOne = new int[columns, lines];
            int[,] arrayTwo = new int[columns, lines];

            Random random = new Random();
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < lines; j++)
                {
                    int randNumber = random.Next(rundMin, rundMax);
                    arrayOne[i, j] = randNumber;
                    Write($"{arrayOne[i, j]} ");
                }
                WriteLine();
            }
            WriteLine();
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < lines; j++)
                {
                    int randNumber = random.Next(rundMin, rundMax);
                    arrayTwo[i, j] = randNumber;
                    Write($"{arrayTwo[i, j]} ");
                }
                WriteLine();
            }
            WriteLine();
            int[,] arrayThree = new int[columns, lines];
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < lines; j++)
                {
                    arrayThree[i, j] = arrayOne[i, j] + arrayTwo[i, j];
                    Write($"{arrayThree[i, j]} ");
                }
                WriteLine();
            }
        }
        /// <summary>
        /// Случайная матрица
        /// </summary>
        void TaskOne()
        {
            WriteLine("Введите количество строк для матрицы.");
            int lines = int.Parse(ReadLine());
            WriteLine("Введите количество столбцов для матрицы.");
            int columns = int.Parse(ReadLine());
            WriteLine($"Ввидите с какого числа начинается подбор случайных чисел.");
            int rundMin = int.Parse(ReadLine());
            WriteLine("Ввидите на каком числе заканчивается подбор случайных чисел.");
            int rundMax = int.Parse(ReadLine());
            rundMax += 1;

            int[,] array = new int[columns, lines];

            Random random = new Random();
            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < lines; j++)
                {
                    int randNumber = random.Next(rundMin, rundMax);
                    array[i, j] = randNumber;
                    Write($"{array[i, j]} ");
                }
                WriteLine();
            }
            //Вывод суммы всех элементов двухмерного массива
            int summa = 0;

            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < lines; j++)
                {
                    summa += array[i, j];
                }
            }
            WriteLine($"Сумма всех элементов двухмерного массива равна:{summa}");

            ReadKey();
        }
    }
}
